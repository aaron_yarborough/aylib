(function () {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };

    var _lib = {
        storage: {
            /**
             * Saves a value with a key in local storage
             * @param {String} Key
             * @param {Any} Value
             */
            set: function (key, val) {
                try {
                    localStorage.setItem(key, JSON.stringify(val));
                } catch (e) {
                    console.warn(("Couldn't save to local storage: {0}").format(e.message));
                }
            },

            /**
             * Gets a saved value from local storage
             * @param {String} Key
             * @returns {Any}
             */
            get: function (key) {
                var obj = JSON.parse(localStorage.getItem(key));

                // If the initial value was not an object, return it instead
                if (_lib.getCount(obj) && obj.hasOwnProperty("data"))
                    return obj["data"];

                return obj;
            }
        },

        /**
         * Gets the number of items in an object
         * @param {Object} Object
         * @returns {Number} Count
         */
        getCount: function (obj) {
            return Object.keys(obj).length;
        }
    };

    window.aylib = _lib;
    window.ajl = _lib;
})();